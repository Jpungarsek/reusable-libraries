import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export enum ECONFIRMATION {
  YES,
  NO,
  CANCEL,
}

@Component({
  selector: 'lib-confirmationDialog',
  template: `
    <kendo-dialog
      [title]="dialogTitle"
      *ngIf="opened"
      [minWidth]="250"
      [width]="450"
      (close)="cancel()"
    >
      <div class="dialog-content">
        {{ content }}
      </div>
      <kendo-dialog-actions>
        <button kendoButton (click)="cancel()">{{ buttonCancelText }}</button>
        <button kendoButton (click)="confirm()" primary="true">
          {{ buttonConfirmText }}
        </button>
      </kendo-dialog-actions>
    </kendo-dialog>
  `,
  styles: [],
})
export class ConfirmationDialogComponent {
  @Input() opened = true;
  @Input() dialogTitle = 'Confirmation Dialog';
  @Input() content?: string;
  @Input() buttonCancelText = 'Cancel';
  @Input() buttonConfirmText = 'Confirm';

  @Output() decisionCanceled = new EventEmitter();
  @Output() decisionConfirmed = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  cancel() {
    this.decisionCanceled.emit();
    this.opened = false;
  }

  confirm() {
    this.decisionConfirmed.emit();
    this.opened = false;
  }
}
