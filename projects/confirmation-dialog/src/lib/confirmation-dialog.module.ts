import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { DialogModule, DialogComponent } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

@NgModule({
  declarations: [ConfirmationDialogComponent],
  imports: [CommonModule, DialogModule, ButtonsModule],
  exports: [ConfirmationDialogComponent],
})
export class ConfirmationDialogModule {}
