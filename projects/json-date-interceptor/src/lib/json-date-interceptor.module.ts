import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { JsonDateInterceptorComponent } from './json-date-interceptor.component';
import { JsonDateInterceptor } from './json-date.interceptor';


@NgModule({
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JsonDateInterceptor, multi: true }
  ],
  declarations: [JsonDateInterceptorComponent],
  imports: [
  ],
  exports: [JsonDateInterceptorComponent]
})
export class JsonDateInterceptorModule { }
