import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class JsonDateInterceptor implements HttpInterceptor {
 // private _isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d*)?Z$/;
 private _apiDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d*)?$/;

 intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

   this.convert(req.body);

   return next.handle(req).pipe(map((val: HttpEvent<any>) => {
     if (val instanceof HttpResponse) {
       this.convert(val.body);
     }
     return val;
   }));
 }

 isApiDateString(value: any): boolean {
   if (value === null || value === undefined) {
     return false;
   }
   if (typeof value === 'string') {
     return this._apiDateFormat.test(value);
   }
   return false;
 }

 convert(body: any) {
   if (body === null || body === undefined) {
     return body;
   }
   if (typeof body !== 'object') {
     return body;
   }
   for (const key of Object.keys(body)) {
     const value = body[key];
     if (this.isApiDateString(value)) {
       body[key] = new Date(value);
       body[key + '_unix'] = Date.UTC(
           body[key].getFullYear(),
           body[key].getMonth(),
           body[key].getDate(),
           body[key].getHours(),
           body[key].getMinutes(),
           body[key].getSeconds()
           );
     } else if (value instanceof Date) {
       body[key] = new Date(Date.UTC(
           value.getFullYear(),
           value.getMonth(),
           value.getDate(),
           value.getHours(),
           value.getMinutes(),
           value.getSeconds()
           ));
     } else if (typeof value === 'object') {
       this.convert(value);
     }
   }
 }
}
