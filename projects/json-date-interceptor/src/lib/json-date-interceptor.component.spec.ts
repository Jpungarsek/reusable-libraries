import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonDateInterceptorComponent } from './json-date-interceptor.component';

describe('JsonDateInterceptorComponent', () => {
  let component: JsonDateInterceptorComponent;
  let fixture: ComponentFixture<JsonDateInterceptorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JsonDateInterceptorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonDateInterceptorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
