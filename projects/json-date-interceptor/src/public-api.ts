/*
 * Public API Surface of json-date-interceptor
 */

export * from './lib/json-date-interceptor.component';
export * from './lib/json-date-interceptor.module';
